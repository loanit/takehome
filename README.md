## LoanIt Takehome test
 
### Getting started

1. Fork the repo.
2. Work on the test.
3. When you are ready to submit, create a pull request back to the original repository at [https://bitbucket.org/loanit/takehome]

### Introduction

This is a java program built using spring-boot and spring-jpa.  

### Objective

Build a RESTful web app that does two things

1. Create an API end point to insert Loans into the database.
2. Create an API end point to fetch all loans from database.
3. Add a sort parameter to the fetch endpoint to sort based on `loanAmount` or `loanTerm`
4. _BONUS_ Add tests
    
### Resources
    
spring web [https://spring.io/guides/gs/rest-service/]

spring jpa [http://www.springboottutorial.com/spring-boot-and-h2-in-memory-database]
