package com.loanit.takehometest;


public class Loan {
    private final long id;
    private final Double loanAmount;
    private final int loanTerm;

    public Loan(long id, Double loanAmount, int loanTerm) {
        this.id = id;
        this.loanAmount = loanAmount;
        this.loanTerm = loanTerm;
    }

    public long getId() {
        return id;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public int getLoanTerm() {
        return loanTerm;
    }
}
