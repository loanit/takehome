package com.loanit.takehometest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TakehomeTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TakehomeTestApplication.class, args);
	}

}

